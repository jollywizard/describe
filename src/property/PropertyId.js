
const PropertyFlags = require('./PropertyFlags')
const PropertyHandle = require('./PropertyHandle')
const describe = require('../describe')
const test = require('../type-tests')

/**
  A class which resolves `(property-site, property-key)` value pairs into an
  access model for configuration information about the property.
*/
class PropertyId {
  constructor(source, token) {
    if (!test.isString(token))
      throw new TypeError(`token must be a string: ${describe.type(id)}`)
    this._source = source
    this._id = token
  }

  /**
    The location where the property can be configured (ie for `Object.defineProperty`).
    @return {instance|prototype|class}
  */
  get source() {return this._source}

  /**
    The id key used to access the property on the source.
  */
  get token() {return this._id}

  /**
    @return {PropertyHandle} A reference that can be used to alter this property.
  */
  get handle() {
    let r = new PropertyHandle()
    r._id = this
    return r
  }

  /**
    An enhanced copy of the configuration object which can be used to submit
    modifications to the property configuration via `Object.defineProperty`.
  */
  descriptor()
  {
    let d = Object.getOwnPropertyDescriptor(
      this.source, this.token
    ) || {configurable:true, exists:false}
    if (d.exists == undefined)
      d.exists = true
    d.token = this.token
    d.source = this.source
    d.tag = this.tag
    return d
  }

  /**
    @return true if the property exists on the source site.
  */
  get exists() {
    return this.descriptor().exists
  }

  /*
    @return true if the property does not have a system configuration.
  */
  get ghost() {
      return !this.exists
  }

  get enumerable()    { return this.descriptor().enumerable}
  get configurable()  { return this.descriptor().configurable}
  get writable()      { return this.descriptor().writable}
  get getter()        { return this.descriptor().get}
  get isGetter()      { return 'get' in this.descriptor()}
  get setter()        { return this.descriptor().set}
  get isSetter()      { return 'set' in this.descriptor()}
  get value()         { return this.descriptor().value}
  get isValue()       { return 'value' in this.descriptor()}

  /* True if it starts with a lowdash */
  get private() {
    return this.token.startsWith('_')
  }

  /** @return true if this property has a value that is not a function */
  get isData() {
    return !test.isFunction(this.value)
  }

  /** @return true if this property has a value that is a function */
  get isMethod() {
    return test.isFunction(this.value)
  }

  /*
    Get an array of strings that describe the properties configuration.
  */
  get flags()
  {
    let r = []
    let desc = this.descriptor() || {}
    for (let k of PropertyFlags)
    {
      if (!!desc[k])
        r.push(k)
    }
    if (this.private)
      r.push('_private')
    if (this.ghost)
      r.push('~ghost')
    return r
  }

  inspect() {
    return this.toString()
  }

  toString() {
    let flags = require('lodash').map(this.flags, str=>str[0])
    return `${describe.type(this._source)}[${this.token}]{${flags}}`
  }
}

module.exports = PropertyId
