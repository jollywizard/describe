
/*
  A property handle uses an id to provide advanced access to
  a property, including the ability to rewrite it by setting
  properties directly on the handle.
*/
class PropertyHandle {

    static forId(propertyId)
    {
      return propertyId.handle
    }

    /*
      @return {PropertyId}
    */
    get id() {return this._id}

    /*
      Get the system descriptor from the PropertyId
    */
    get descriptor() {
      return this.id.descriptor()
    }

    get enumerable()    { return this.id.enumerable}
    get configurable()  { return this.id.configurable}
    get writable()      { return this.id.writable}
    get getter()        { return this.id.get}
    get isGetter()      { return this.id.isGetter}
    get setter()        { return this.id.set}
    get isSetter()      { return this.id.isSetter}
    get value()         { return this.id.value}

    update(command) {
      let config = this.descriptor
      if (command instanceof Function)
      {
        command(config)
      }
      else
      {
        for (let k in command)
          config[k] = command[k]
      }
      SetPropertyDescriptor(this.id, config)
    }

    set enumerable(bool) {
      this.update((config) => {config.enumerable = bool})
    }

    set value(value) {
      this.update((config) => {config.value = value})
    }

    set getter(func) {
      this.update((config) => {
        config.get = func
      })
    }

    set setter(func) {
      this.update((config) => {config.set = func})
    }

    remove() {
      //TODO: Use defineProperties to resubmitted trimmed list.
      throw 'Not implemented yet'
    }
}


function SetPropertyDescriptor(propertyId, config)
{
  return Object.defineProperty(
    propertyId.source, propertyId.token, config
  )
}

module.exports = PropertyHandle
