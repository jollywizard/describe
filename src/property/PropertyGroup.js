const _ = require('lodash')

const test = require('@jamesarlow/isit')

const System = require('../system')
const PropertyId = require('./PropertyId')
const PropertyHandle = require('./PropertyHandle')
const Type = require('../type')
const describe = require('../describe')

/**
  Wraps a group of properties as a single object to make the configurable as a group.
*/
class PropertyGroup {
  static of(site)
  {
    /* By default all system names (chain with lodash for more specificity)*/
    return new PropertyGroup(site, System.names.of(site))
  }

  constructor(source, ...tokens)
  {
    this.source = source
    this.tokens = tokens
  }

  get ids()
  {
    return this._ids
  }

  get chainIds() {
    return _.chain(this.ids)
  }

  get chainHandles() {
    return this.chainIds
    .map(PropertyHandle.forId)
  }

  get handles()
  {
    return this.chainHandles.value()
  }

  get enumerable()
  {
    return new PropertyGroup(this.source
    , this.chainIds
      .filter((id)=>id.enumerable)
      .value()
    )
  }

  get getters()
  {
    const newIds = this.chainIds
          .filter(id => id.isGetter)
          .value()
    return new PropertyGroup(this.source, ...newIds)
  }

  get private()
  {
    const newIds = this.chainIds
      .filter(id => id.private)
      .value()
    return new PropertyGroup(this.source, ...newIds)
  }

  get values()
  {
    const newIds = this.chainIds
      .filter(id => id.isValue)
      .value()
    return new PropertyGroup(this.source, ...newIds)
  }

  get methods()
  {
    const newIds = this.chainIds
      .filter(id => id.isMethod)
      .value()
    return new PropertyGroup(this.source, ...newIds)
  }

  get data()
  {
    const newIds = this.values.chainIds
      .filter(id => id.isData)
      .value()
    return new PropertyGroup(this.source, ...newIds)
  }

  set tokens(tokens)
  {
    if (tokens.length == 0)
      return
    else
    /* This catch will downconvert a missing spread operator (i.e. [[...]]=>[...]).*/
    if (tokens.length == 1 && tokens[0] instanceof Array)
      tokens = [...tokens[0]]

    this._ids = _.map(tokens, (token) => {
      if (token instanceof PropertyId)
        return token
      if (token instanceof PropertyHandle)
          return token.id
      return new PropertyId(this.source, token)
    })
    this._tokens = _.map(this._ids, (id) => id.token)
  }

  update(config)
  {
    return _.map(this.handles, (handle) => handle.update(config))
  }

  set enumerable(bool) {
    this.update({enumerable:bool})
  }

  [Symbol.iterator] () {
    return this.handles[Symbol.iterator]()
  }

  inspect() {return this.toString()}

  toString() {
    let me=describe.type(this)
        me=describe.decorate(me, ()=>{})
    let type = describe.type(this.source)
    let tokens = this._tokens
    return `${me}<${type}>[${tokens}]`
  }
}

module.exports = PropertyGroup
