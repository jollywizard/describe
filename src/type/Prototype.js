const TypeTag = require('./TypeTag')

class Prototype extends TypeTag {

  static metaType () {return 'Prototype'}
}

module.exports = Prototype
