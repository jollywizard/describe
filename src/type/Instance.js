const TypeTag = require('./TypeTag')
const Type = require('./Type')

class Instance extends TypeTag {
  constructor(instance)
  {
    super(Type.ofInstance(instance), instance)
    this._value = instance
  }

  get value() {return this._value}

  toString() {
    return `Instance<${this.typename}>`
  }
}

module.exports = Instance
