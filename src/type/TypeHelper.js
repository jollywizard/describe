/*
  Extends Type to provide accessors for non-type-tag helpers
*/
class TypeHelper {
  constructor(type)
  {
    this._type = type
  }

  get type()        { return this._type }
  /*
    Get the enumerable keys from the type. (i.e. `Object.keys(...)`)
  */
  get keys()        { return keys(this.type.classFunction) }
  /*
    Get the keys for all properties on the prototype
  */
  get tokens()      { return Object.getOwnPropertyNames(this.type.prototype) }

  /*
    Gets property descripotrs for all tokens.
  */
  get properties()  {
    return this.tokens.map((t) => this.property(t))
  }

  property(id) {
    let descriptor = Object.getOwnPropertyDescriptor(this.type.prototype, id)
    descriptor.name = id
    return descriptor
  }

  get getters() {
    return this.properties.filter((prop) => !!prop.get)
  }

  get fields()      {/* values ++ getters, not statics */}
  get values()      {/* value fields, not statics */}
  get methods()     {/* value is function */}
}
