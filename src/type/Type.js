const System = require('../system')
const TestType = require('../type-tests')
const TypeTag = require('./TypeTag')

/*
  A type descriptor for a class, that clarifies the confusing circular loop:
  `classFunction == classFunction.prototype.constructor`.

  Create with `new Type(classLiteral)`, e.g. `class A {}; let type = new Type(A)`.
*/
class Type extends TypeTag {
  constructor(clazz) {
    super()
    if (!TestType.isClass(clazz)) {
      throw new TypeError(`Not a class: ${clazz}`)
    }
    this._classFunction = clazz

    this._type = this
    this._site = this.prototype
  }

  get metaType() {return "Type"}

  static ofClass(clazz) {return new Type(clazz)}

  static ofInstance(object)
  {
    return new Type(System.class.of(object))
  }

  static detect(any)
  {
    return new Type(Class(any))
  }

  /*
    This is the function that was used to declare the object.
    e.g. `class A {} -> [Function A]`

    This is the literal value you would use to declare an instance of Type.
    e.g. `class A {}; let type = new Type(A)`

    Should be equivalent to the constructor function.
    (i.e. `classFunction.prototype.newFunction`)
  */
  get classFunction() {return this._classFunction}

  get name() {return this.classFunction.name}
  get typename() {return this.name}


  /*
    The prototype object for this type.
  */
  get prototype() {return this.classFunction.prototype}

  /*
    The constructor reference from `prototype`.

    This should be the same as the classFunction, but is
    calculated by: `classFunction.prototype.constructor`.
    Mileage may vary.
  */
  get newFunction() {return this.prototype.constructor}

  /*
    @return A {TypeHelper} for this class, which can be used to
      retrieve reflection information.
  */
  get helper() {return new TypeHelper(this)}

  get instanceof() {
    return function(test) {return test instanceof this.classFunction}
  }

  toString() {
    return `Type<${this.typename}>`
  }

  property(token) {
    let prop = Property.Id(this.prototype, token)
    prop.tag = this
    return prop
  }
}

module.exports = Type
