
class TypeTag {
  constructor(type, site)
  {
    this._type = type
    this._site = site
  }

  get metaType() {return 'TypeTag'}

  get type() {
    return this._type
  }

  get typename() {
    return this.type.typename
  }

  get keys() {
    return System.keys.of(this._site)
  }

  get propertyNames () {
    return System.names.of(this._site)
  }

  get properties () {
    return _.map(this.propertyNames, n => {return this.property(n)})
  }

  property(token)
  {
    return Property.Id(this._site, token)
  }
}

module.exports = TypeTag
