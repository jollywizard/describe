const _ = require('lodash')
const TestType = require('@jamesarlow/isit')
const System = require('./system')

/*
  @return An iterator for prototype hierarchy of the target.
*/
function* PrototypeChain(target)
{
  let prototype = System.prototype.of(target)
  while(!!prototype)
  {
    yield prototype
    prototype = System.prototype.of(prototype)
  }
}

/*
  Traverses each of the property value sites of the target (i.e. `target ++ ProrotypeChain(target)``)
*/
function* ValueChain(target)
{
  yield target
  yield* PrototypeChain(target)
}

function* ClassChain(target) {
  let prototype = System.prototype.for(target)

  while(prototype)
  {
    yield prototype.constructor
    prototype = System.prototype.of(prototype)
  }
}

function NameChain(target)
{
  const sites = [...ValueChain(target)]
  return _.map(sites, (site)=>{
    return System.names.of(site)
  })
}

function KeyChain(target)
{
   const sites = [...ValueChain(target)]
   return _.map(sites, (site) => {
     return System.keys.of(site)
   })
}

function DescriptorChain(target)
{
  const sites = [...ValueChain(target)]
  return _.map(sites, (site) => {
    return System.descriptors.of(site)
  })
}

module.exports = {
   classes:ClassChain
,  prototypes:PrototypeChain
,  values:ValueChain
,  names:NameChain
,  keys:KeyChain
,  descriptors:DescriptorChain
}
