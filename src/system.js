const TestType = require('@jamesarlow/isit')

/**
  Get the prototype for an object using the system helper.

  Note: For functions, this will return `Function`, To get the prototype declared
  by a class instead, use `PrototypeFor`

  @param {any} target The value whose prototype will be retrieved.
  @return {prototype} The prototype of the target.
*/
function PrototypeOf(target)
{
  return Object.getPrototypeOf(target)
}

/**
  Gets the prototype for an object, or the prototype created by a class.

  Objects are treated the same. Class functions are a special case, because
  the prototype of a function is `Function`.  This function will get the prototype
  declared by the class instead.

  @param {class|object} target The class or object to get the prototype for.
  @return {prototype} The target or prototype of the target.
*/
function PrototypeFor(target)
{
  if (TestType.isClass(target))
  {
    return target.prototype
  }
  if (TestType.isObject(target))
  {
    return PrototypeOf(target)
  }
}

/**
  Get the class of an object. Works for instances and prototypes.

  @param {object} object
  @return the class function for `object`
*/
function ClassOf(object)
{
  return object && object.constructor
}

/**
  If the type is a class, return it class, otherwise get the class for the object.

  @param {any} any
*/
function ClassFor(any)
{
  const target = any
  if (TestType.isClass(target))
  {
    return target
  }
  if (TestType.isArray(target))
  {
    return Array
  }
  if (TestType.isObject(target))
  {
    return ClassOf(target)
  }
  return Function
}

/**
  Get the type name string for a class or object.

  @param {class|object} any
  @return The string function name of the class or associated class.
*/
function Typename(any)
{
  return ClassFor(any).name
}

/**
  Get the property keys for the property site

  @param {any} propertySite.
*/
function NamesOf(propertySite)
{
  return Object.getOwnPropertyNames(propertySite)
}

/**
  Gets the enumerable keys for the property site.

  For all property keys, use `NamesOf`

  @param {any} site : The property site.
  @return {String[]} : The enumerable keys local to the property site.
*/
function KeysOf()
{
  return Object.keys(any)
}

/**
  Get the property descriptors for the property site.

  @param {any} site : The property site.
  @return {Object[]} : An array of property descriptor objects.
*/
function DescriptorsOf(any)
{
  return Object.getOwnPropertyDescriptors(any)
}

module.exports = {
  prototype : {
    ['of'] : PrototypeOf
  , ['for'] : PrototypeFor
  }
, ['class'] : {
    ['of'] : ClassOf
  , ['for'] : ClassFor
  }
, typename : Typename
, names : {
    ['of'] : NamesOf
  }
, keys : {
    ['of'] : KeysOf
  }
, descriptors : {
    ['of'] : DescriptorsOf
  }
}

const funcs = [
  PrototypeOf, PrototypeFor
, ClassOf, ClassFor
, Typename
, NamesOf, KeysOf, DescriptorsOf
]
for (let func of funcs)
  module.exports[func.name] = func
