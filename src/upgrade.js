const _ = require('lodash')

const Property = require('./property')
const TypeTest = require('@jamesarlow/isit')
const Hierarchy = require('./hierarchy')
const describe = require('./describe')

/*
  Installs `describeMe` as the `toString` and `inspect` handler.
*/
function describify(target) {
  let targets = [target]
  if (TypeTest.isClass(target))
    targets.push(target.prototype)

  for (let site of targets)
  for (let key of ['inspect', 'toString'])
  {
    Object.defineProperty(site, key,
    {
      enumerable:false, configurable:true, value:describe.me
    })
  }
}

function* iterator(obj) {
  for (const k of [...Hierarchy.keys(obj)])
  {
    yield {key:k, value: obj[k]}
  }
}

function InstallIterable(clazz) {
  let prototype = clazz.prototype
  let func = Symbol.iterator
  prototype[func] = function() {
      return iterator(this)
  }
}

function EnumerateGettersOf(site)
{
  const getters = Property.Ids.of(site).getters
  getters.enumerable = true
}

function EnumerateGettersFor(any)
{
  let target = any
  if (TypeTest.isClass(target))
  {
    const clazz = target
    EnumerateGettersOf(clazz.prototype)
    return
  }
  EnumerateGettersOf(target)
}

function HidePrivateOf(site)
{
  const props = Property.Ids.of(site).enumerable.private
  props.enumerable = false
}

function HidePrivateFor(any)
{
  const target = any
  if (TypeTest.isClass(target))
  {
    const clazz = target
    HidePrivateOf(clazz.prototype)
    return //TODO: Solve crash when this is removed.
  }
  HidePrivateOf(target)
}

function wireup(clazz) {
    describify(clazz)
    InstallIterable(clazz)
    EnumerateGettersFor(clazz)
    HidePrivateFor(clazz)
}

module.exports = wireup
let e = module.exports

e.describify = describify
e.hidePrivate = {
  of : HidePrivateOf
, for : HidePrivateFor
}
