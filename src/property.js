const _ = require('lodash')
const System = require('./system')

const PropertyFlags = require('./property/PropertyFlags')
const PropertyId = require('./property/PropertyId')
const PropertyHandle = require('./property/PropertyHandle')
const PropertyGroup = require('./property/PropertyGroup')

function Enumerate(bool, clazz, ...tokens)
{
  const group = new PropertyGroup(clazz, ...tokens)
  group.enumerable = true
}

const e = module.exports

e.class = {
  PropertyId : PropertyId
, PropertyGroup : PropertyGroup
, PropertyHandle : PropertyHandle
}

e.Id = (...args) => new PropertyId(...args)
e.Ids = PropertyGroup
e.enumerate = Enumerate
e.flags = PropertyFlags
