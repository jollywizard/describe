const _ = require('lodash')

const TypeType = require('@jamesarlow/isit')

const TestType = require('./type-tests')
const TypeTag = require('./type/TypeTag')
const TypeHelper = require('./type/TypeHelper')
const Instance = require('./type/Instance')
const Prototype = require('./type/Prototype')

function Tag(any)
{
  if (any instanceof TypeTag)
  {
    return any
  }
  if (TestType.isClass(any))
  {
    return new Type(any)
  }
  if (TestType.isObject(any))
  {
    return new Instance(any)
  }
}


module.exports = TypeType
const e = module.exports

e.class = {}
const c = e.class

c.TypeTag = TypeTag
c.Type = TypeType
c.TypeHelper = TypeHelper
c.Prototype = Prototype
c.Instance = Instance

e.Tester = TestType
e.tag = Tag

e.Instance = (instance) => {return new Instance(instance)}
