const colors = require('colors')
const inspect = require('util').inspect

const TestType = require('@jamesarlow/isit')
const System = require('./system')
const Type = require('./type')

/**
  A function that returns `describe(this)`.

  Use to enhance toString/inspect.
*/
function describeMe() {
  return describe(this)
}

/*
  A function that uses the enhanced reflection from this package to
  provide enhanced object descriptions.
*/
function describe(target, opts) {
  if (!target)
  {
    return inspect(target)
  }
  if (TestType.isClass(target))
    return describeClass(target, opts)
  if (TestType.isObject(target))
    return describeObject(target, opts)
  return `${describeType(target)} ${inspect(target, {colors:true})}`
}

/*
  Get a formatted type tag.  Formatting will indicate class/prototype/instance
  status.
*/
function describeType(any)
{
  return decorate(System.typename(any), any)
}

/*
  Describe class and prototype.  Decorate as class.
*/
function describeClass(clazz)
{
  if (!TestType.isClass(clazz))
    throw new TypeError(`Parameter should be a class: ${clazz}`)

  let msg = `${describeType(clazz)} ${describeProperties(clazz.prototype)}`
  return msg
}

/*
  Describe the property list.
*/
function describeProperties(any)
{
  const useArray = any instanceof Array
  const r = useArray ? [] : {}, site = any
  for (let i in site)
  {
    r[i] = site[i]
  }
  return inspect(r)
}

/*
  Describe any object as it's type and properites.
*/
function describeObject(object)
{
  const type = describeType(object)
  return `${type} ${describeProperties(object)}`
}

function decorate(text, type)
{
  let r = text
  if (TestType.isFunction(type))
  {
    if (describe.useColor)
      r = colors.italic(r)
    if (TestType.isClass(type))
    {
      if (describe.useColor)
        r = colors.bold(r)
      if (describe.useText)
        r = `${r}.class`
    }
    else if (describe.useText)
      r = `${r}.function`
  }

  if (TestType.isPrototype(type))
  {
    if (describe.useColor)
      r = colors.underline(r)
    if (describe.useText)
      r = `${r}.prototype`
  }

  return r
}

describe.useColor = true
describe.useText = false

module.exports = describe

describe.class = describeClass
describe.type = describeType
describe.object = describeObject
describe.properties = describeProperties
describe.me = describeMe
describe.decorate = decorate
