
const src = '../src'

const system = require(`${src}/system`)

const assert = require('assert')

class A {}
const a = new A()

class B extends A{}
const b = new B()

class O extends Object{}

describe('Module: `system`', function() {

  it('offers camel case and point delimited namespaces.', ()=>{
    assert.equal(system.ClassFor, system.class.for)
    assert.equal(system.ClassOf, system.class.of)
    assert.equal(system.PrototypeFor, system.prototype.for)
    assert.equal(system.PrototypeOf, system.prototype.of)
    assert.equal(system.Typename, system.typename)
    assert.equal(system.KeysOf, system.keys.of)
    assert.equal(system.NamesOf, system.names.of)
    assert.equal(system.DescriptorsOf, system.DescriptorsOf)
  })

  describe('Class awareness', function() {
    it('knows a class extends Function', ()=>{
      assert.equal(system.class.of(A), Function)
    })
    it('can resolve an object to its class', ()=> {
      assert.equal(system.class.of(a), A)
    })
    it('can normalize results so instance matches class.', ()=>{
      assert.equal(system.class.of(a), system.class.for(a))
      assert.equal(system.class.for(A), system.class.for(a))
    })
  })

  describe('Prototype awareness', ()=>{
    it('knows the prototype of a class is its supertype function.', ()=>{
      assert.equal(system.prototype.of(A), Function.prototype)
      assert.equal(system.prototype.of(B), A)
      assert.equal(system.prototype.of(O), Object)
    })

    it('can get the prototype declared for the class instead', function() {
      assert.equal(system.prototype.for(A), A.prototype)
      assert.equal(system.prototype.for(B), B.prototype)
    })

    it('can get the prototype of an instance using either method.', function() {
      assert.equal(A.prototype, system.prototype.of(a))
      assert.equal(A.prototype, system.prototype.for(a))
    })
  })

  describe('Typename awareness', ()=>{
    it('resolves an instance, class, or prototype to the class name string.', ()=> {
        let type = A.name
        assert.equal(system.typename(A), type)
        assert.equal(system.typename(a), type)
        assert.equal(system.typename(A.prototype), type)
    })
  })
})
