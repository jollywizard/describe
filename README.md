# describe
*Enhanced Reflection and toString for Node*

## Overview

`describe` offers:

* Class name resolution
* Decorators to differentiate between a Class, Prototype, Instance, and Anonymous Functions
  * An alternative stringifier which uses decorators to differentiate the target by type.
* A normalized access scheme for common object properties: [class, prototype, source, keys, names, descriptors]
* Iterables for traversing object hierarchies (class, prototype, name, descriptors)
* Reflection model for property configuration(s)
    * Control individual properties or groups.
    * Simplify grouping, access, and configuration.
    * Extra flags: 
        * `~ghost`: does not exist 
        * `_private`: starts with `_`
* Upgrades for existing objects, classes, and prototypes:
    * Enhanced toString/inspect
    * Instance getters become enumerable
    * Iterable via `in` keys

## Installation

`npm install @jw/describe`

## Usage

The API is still congealing, but the two key uses cases should stay the same from hereout:

### Use `describe` to decorate types.

The main describe function generates toString/inspect descriptions, but with decorators to differentiate between class, prototype, instance, and anonymous functions.

By default, decorators use terminal control codes, which node may escape making output look jarbled.

For the most effective functional use case, make sure to call `console.log` on the explicitly stringified value.

Alternatively: use `describe.useText = true & describe.useColor = false`, to get inline text decorators instead.

```
const describe = require('.')
class A {}
let a = new A()
let anon = ()=>{}

console.log(`${describe(A)}`)
console.log(`${describe(A.prototype)}`)
console.log(`${describe(a)}`)
console.log(`${describe(anon)}`)
```

### Use `describe.upgrade(...)` to enhance toString()

```
const describe = require('.')

class A {
   constructor() {this.one = 1}
   get uno() {return 1}
}
describe.upgrade(A)

let a = new A()

console.log(A)
console.log(`${A.prototype}`) /* node does not toString/inspect prototypes, so use a template string */
console.log(a)
```