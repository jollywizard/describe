const src = './src'

const describe = require(`${src}/describe`)
const system = require(`${src}/system`)
const property = require(`${src}/property`)
const upgrade = require(`${src}/upgrade`)

/*
  TODO Install describify on exported classes in the exports section.
  (avoid circular depend)
*/

module.exports = describe
const e = module.exports

e.system = system
e.property = property
e.upgrade = upgrade
